from sklearn.metrics import mean_absolute_error, mean_squared_error, \
    mean_squared_log_error, median_absolute_error, explained_variance_score, \
    max_error, r2_score, mean_poisson_deviance, mean_gamma_deviance, \
    mean_tweedie_deviance, accuracy_score, balanced_accuracy_score, \
    recall_score, precision_score, f1_score, zero_one_loss, hamming_loss, \
    jaccard_score, matthews_corrcoef, roc_auc_score, average_precision_score, \
    brier_score_loss, coverage_error, label_ranking_average_precision_score, \
    label_ranking_loss, ndcg_score, adjusted_mutual_info_score, \
    adjusted_rand_score, completeness_score, fowlkes_mallows_score, \
    homogeneity_score, mutual_info_score, normalized_mutual_info_score, \
    v_measure_score, calinski_harabasz_score, davies_bouldin_score, \
    silhouette_score

SKLEARN_REGRESSOR_METRICS = [
    mean_absolute_error, mean_squared_error, mean_squared_log_error,
    median_absolute_error, explained_variance_score, max_error, r2_score,
    mean_poisson_deviance, mean_gamma_deviance, mean_tweedie_deviance]
SKLEARN_BINARY_CLASSIFIER_METRICS = [
    accuracy_score, balanced_accuracy_score, recall_score, precision_score,
    f1_score, zero_one_loss, hamming_loss, jaccard_score, matthews_corrcoef]
SKLEARN_CLASSIFIER_PROBABILITY_METRICS = [
    roc_auc_score, average_precision_score, brier_score_loss]
SKLEARN_MULTICLASS_CLASSIFIER_METRICS = [
    accuracy_score, balanced_accuracy_score, zero_one_loss, hamming_loss,
    matthews_corrcoef]
SKLEARN_MULTICLASS_AVERAGING_METRICS = [
    f1_score, recall_score, precision_score, jaccard_score]
SKLEARN_MULTILABEL_CLASSIFIER = [
    accuracy_score, zero_one_loss, hamming_loss]
SKLEARN_MULTILABEL_AVERAGING_METRICS = [
    f1_score, recall_score, precision_score, jaccard_score,
    average_precision_score]
SKLEARN_MULTILABEL_RANKING_METRICS = [
    coverage_error, label_ranking_average_precision_score,
    label_ranking_loss, ndcg_score]
SKLEARN_UNSUPERVISED_CLUSTERING_METRICS = [
    adjusted_mutual_info_score, adjusted_rand_score, completeness_score,
    fowlkes_mallows_score, homogeneity_score, mutual_info_score,
    normalized_mutual_info_score, v_measure_score]
SKLEARN_CLUSTERING_SPECIFIC_METRICS = [
    calinski_harabasz_score, davies_bouldin_score, silhouette_score]
