"""
This is the implementation of sklearn component
"""

__all__ = ["AbstractSklearnComponent"]

import itertools
import os
import pickle
import sys
from itertools import cycle

import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import shap
import sklearn
import sklearn.model_selection
import sklearn.utils.multiclass
from sklearn.exceptions import ConvergenceWarning
from sklearn.model_selection import validation_curve
from sklearn.preprocessing import label_binarize
from sklearn.utils._testing import ignore_warnings

from xpresso.ai.core.commons.utils.constants import EMPTY_STRING, \
    DEFAULT_VALIDATION_SIZE, RANDOM_STATE
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.ml_utils.generic import false_negative_rate, \
    false_positive_rate, true_negative_rate, format_metric_key
from xpresso.ai.core.ml_utils.shap import shap_plots_single_output, return_name, \
    shap_generate_feature_names
from xpresso.ai.core.ml_utils.sklearn import \
    SKLEARN_BINARY_CLASSIFIER_METRICS, SKLEARN_CLASSIFIER_PROBABILITY_METRICS, \
    SKLEARN_CLUSTERING_SPECIFIC_METRICS, SKLEARN_MULTICLASS_AVERAGING_METRICS, \
    SKLEARN_MULTICLASS_CLASSIFIER_METRICS, SKLEARN_MULTILABEL_CLASSIFIER, \
    SKLEARN_MULTILABEL_AVERAGING_METRICS, SKLEARN_MULTILABEL_RANKING_METRICS, \
    SKLEARN_REGRESSOR_METRICS, SKLEARN_UNSUPERVISED_CLUSTERING_METRICS
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

shap.initjs()
MOUNT_PATH = os.environ.get("XPRESSO_MOUNT_PATH", None)
METRICS_FOLDER = os.path.join(MOUNT_PATH, "reported_data/metrics")
PLOTS_FOLDER = os.path.join(MOUNT_PATH, "reported_data/plots")
SHAP_FOLDER = os.path.join(MOUNT_PATH, "reported_data/explainability")


class AbstractSklearnComponent(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from
    AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is
       initiated.
          This method has a single parameter - the experiment run ID. This is
          automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.
    """
    def __init__(self, name, run_name, params_filename,
                 params_commit_id):
        super().__init__(name=name,
                         run_name=run_name,
                         params_filename=params_filename,
                         params_commit_id=params_commit_id)
        self.model = None
        self.x_train = None
        self.y_train = None
        self.calculated_metrics = {}
        self.metric_suffix = EMPTY_STRING
        self.explainer = None
        # create a folder to save images or csv files generated from calling
        # report metrics method
        os.makedirs(METRICS_FOLDER, exist_ok=True)
        os.makedirs(PLOTS_FOLDER, exist_ok=True)
        os.makedirs(SHAP_FOLDER, exist_ok=True)

    def completed(self, push_exp=False, success=True,
                  generate_validation_metrics=True,
                  validation_size=DEFAULT_VALIDATION_SIZE):
        """
        The completed method for sklearn component calls the
        report_sklearn_metrics
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases
            generate_validation_metrics : Boolean to generate validation set
            validation_size:The percentage is used for the test_size
                parameter of sklearn train_test_split if
                generate_validation_metrics is True
        """
        try:
            if not (self.x_train and self.y_train and self.model):
                print("Model, x_train or y_train data not "
                      "provided for metric calculation")
            else:
                self.report_sklearn_metrics(self.x_train, self.y_train,
                                            generate_validation_metrics,
                                            validation_size)
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def report_sklearn_metrics(self, x_values, y_values,
                               generate_validation_metrics=True,
                               validation_size=DEFAULT_VALIDATION_SIZE):
        """
        The report_metrics method collects, calculate and report all metrics
        available in SKlearn library.
        The method detects user's model type (regressor, classifiers-binary,
        classifiers-multiclass, classifiers-multilabel, unsupervised-clustering)
        Args:
            x_values (array_like or sparse matrix): input matrix (either
                training
            set, validation set, or test set) with shape of (n_samples,
            n_features)
            y_values : array-like of shape (n_samples,) or (n_samples,
            n_classes)
                target variable vector or matrix (either training set,
                validation set, or test set)
            generate_validation_metrics : boolean (default=True) If True,
                training and validation set metrics are generated. Metrics with
                suffix ('_tr', '_val') represent training set and validation set
                metrics respectively.
            validation_size: float (default=0.2).
                            The percentage is used for the test_size
                            parameter of sklearn train_test_split if
                            generate_validation_metrics is True
        """
        if not isinstance(self.model, sklearn.base.BaseEstimator):
            self.logger.error("Model not built or invalid type")
            return
        if generate_validation_metrics:
            try:
                x_train, x_val, y_train, y_val = \
                    sklearn.model_selection.train_test_split(
                        x_values, y_values, test_size=validation_size,
                        random_state=RANDOM_STATE)
                print(f"Generated validation set with validation size:"
                      f"{validation_size}, random state:{RANDOM_STATE}")
                # report metrics for training and validation set separately
                for x_data, y_data, dataset_name in [
                    (x_train, y_train, " (Tr)"), (x_val, y_val, " (Val)")]:
                    # make prediction and report metrics
                    self.predict_and_evaluate(x_data, y_data, dataset_name)
            except Exception:
                generate_validation_metrics = False
                self.logger.exception(
                    "Failed to split dataset into train and validation")
        if not generate_validation_metrics:
            self.predict_and_evaluate(x_values, y_values)
        print("Reporting metrics to xpresso.ai")
        # send metrics to xpresso UI
        self.report_kpi_metrics(self.calculated_metrics)
        self.logger.exception("Metrics Reported")

    def predict_and_evaluate(self, x_values, y_values,
                             dataset_name=EMPTY_STRING):
        """
        Predict using self.model and calculate the metrics
        Args:
            x_values: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y_values: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set,
                validation set, or test set)
            dataset_name: string (default "")
                The value will be "tr" and "val", indicating training set,
                validation set, if generate_validation_metrics
                is True.
        """
        self.metric_suffix = dataset_name
        # make predictions
        y_pred, y_pred_proba, y_score = self.generate_prediction(x_values)
        # populate metrics
        self.populate_metrics(x_values, y_pred, y_values, y_pred_proba, y_score)

    def generate_prediction(self, x_values):
        """
        Generate prediction using model.predict()
        Returns: The output from running sklearn model.predict() (labeled as
        y_pred). For sklearn classification models that have
        "decision_function" or "predict_proba" methods, generate predicted
        probabilities or predicted decisions
        """
        print("Generating predictions for given dataset")
        # Apply trained model to make predictions on given dataset
        y_pred = self.model.predict(x_values)
        y_score = None
        y_pred_proba = None
        # check if the trained model has decision_function
        if hasattr(self.model, "decision_function"):
            y_score = self.model.decision_function(x_values)
        # Check if the trained model can predict probability
        if hasattr(self.model, "predict_proba"):
            y_pred_proba = self.model.predict_proba(x_values)
        return y_pred, y_pred_proba, y_score

    def populate_metrics(self, x_values, y_pred, y_true, y_pred_proba, y_score):
        """
        Returns: populate metrics based on model type (regressor, classifier,
        or unsupervised clustering)
        """
        print("Populating metrics")
        # check if the model is sklearn regressor
        if sklearn.base.is_regressor(self.model):
            self.populate_metrics_regressor(y_pred, y_true)
        # check if the model is a sklearn classifier
        elif sklearn.base.is_classifier(self.model):
            self.populate_metrics_classifier(x_values, y_pred, y_true,
                                             y_pred_proba, y_score)
        # check if the model is a unsupervised clustering algorithm within
        # sklearn.cluster module
        elif self.model.__class__.__name__ in sklearn.cluster.__all__:
            self.populate_metrics_unsupervised_clustering(x_values,
                                                          y_pred, y_true)

    def calculate_metrics(self, metrics_list, value_1, value_2,
                          avg_method=None):
        """
        Calculates the value for list of metrics with given data.
        Args:
             metrics_list: list of sklearn metrics
             value_1: y_true data
             value_2: y_pred data
             avg_method(str optional): avg_method to use for metric evaluation
        """
        for metric in metrics_list:
            key_val = f"{self.metric_suffix}_{format_metric_key(metric)}"
            try:
                if not avg_method:
                    self.calculated_metrics[key_val] = round(
                        metric(value_1, value_2), 4)
                    continue
                self.calculated_metrics[f'{key_val}_{avg_method}'] = round(
                    metric(value_1, value_2, avg_method=avg_method), 4)
            except Exception as exp:
                self.logger.exception(
                    f"Unable to calculate metric {metric}. Reason:{str(exp)}")

    def populate_metrics_regressor(self, y_pred, y_true):
        """
        Populate metrics for regression models using sklearn metrics APIs
        Returns: metrics values and metrics name
        """
        print("Populating regressor metrics")
        self.calculate_metrics(SKLEARN_REGRESSOR_METRICS, y_pred, y_true)

    def populate_metrics_classifier(self, x_values, y_pred, y_true,
                                    y_pred_proba, y_score):
        """
        Populate metrics, save classification report (as csv file) and plot
        confusion matrix, roc curve and
        precision_recall curve for all three type of classifiers ((binary,
        multiclass or multilabel).
        SKlearnComponent leverage metrics available from sklearn library. The
        metrics that will be
        finally reported depends on the classifier type (binary, multiclass
        or multilable)
        Returns: metrics values and metrics name
        """
        # identify target_type
        target_type = sklearn.utils.multiclass.type_of_target(y_true)
        if target_type not in ["multiclass", "multiclass-multioutput",
                               "multilabel-indicator", "binary"]:
            return
        # identify model classes
        try:
            model_classes = self.model.classes_
        except AttributeError:
            model_classes = sklearn.utils.multiclass.unique_labels(y_true)
        # mutliclass or multilabel indicators
        multi_ind = ["multiclass", "multiclass-multioutput",
                     "multilabel-indicator"]
        # save classification report for all types
        self.save_classification_report(y_pred, y_true)
        # Calculate metrics that use y_true and y_score as inputs
        if y_score:
            score_metrics = [sklearn.metrics.hinge_loss]
            self.calculate_metrics(score_metrics, y_true, y_score)
        # Calculate metrics that use y_true and y_pred_proba as inputs
        if y_pred_proba:
            proba_metrics = [sklearn.metrics.log_loss]
            self.calculate_metrics(proba_metrics, y_true, y_pred_proba)
        # check if the model is binary classifier:
        if target_type == "binary" or len(model_classes) == 2:
            self.populate_metrics_binary(y_pred, y_true, y_pred_proba)
            # plot roc curve for binary classifier
            self.plot_roc_curve_binary(x_values, y_pred, y_true, y_pred_proba)
            # plot precision_recall_curve for binary classifier
            self.plot_precision_recall_curve_binary(x_values, y_pred, y_true,
                                                    y_score)
            # plot confusion matrix for binary or multiclass classification task
            self.plot_confusion_matrix(x_values, y_pred, y_true)
        # check if the model is a multiclass or multilabel classifier
        elif target_type in multi_ind or len(model_classes) > 2:
            # plot roc curve for multiclass or multilabels
            self.plot_roc_curve_multi(y_pred, y_true, y_score)
            # plot precision recall curve for multiclass or multilabels
            self.plot_precision_recall_curve_multi(y_pred, y_true,
                                                   model_classes, y_pred_proba)
            # for multiclass classification
            if not sklearn.utils.multiclass.is_multilabel(y_true):
                self.populate_metrics_multiclass(y_pred, y_true, y_pred_proba)
                # plot confusion matrix for binary or multiclass
                # classification task
                self.plot_confusion_matrix(x_values, y_pred, y_true)
            # for multilabel classification
            elif sklearn.utils.multiclass.is_multilabel(y_true):
                self.populate_metrics_multilabel(y_pred, y_true,
                                                 y_pred_proba, y_score)
                # plot multilabel confusion matrix for all types of
                # classification task
                self.plot_multilabel_confusion_matrix(y_pred, y_true)

    def populate_metrics_binary(self, y_pred, y_true, y_pred_proba):
        """
        Populate metrics for binary classifiers using both sklearn library.
        Returns: metrics values and metrics name
        """
        self.calculate_metrics(SKLEARN_BINARY_CLASSIFIER_METRICS, y_pred,
                               y_true)
        # add additional metrics from confusion matrix
        try:
            true_negative, false_positive, false_negative, true_positive = \
                sklearn.metrics.confusion_matrix(y_true, y_pred).ravel()
            temp_metrics = {
                f"true_negative_rate{self.metric_suffix}":
                    true_negative_rate(true_negative, false_positive),
                f"false_positive_rate{self.metric_suffix}":
                    false_positive_rate(false_positive, true_negative),
                f"false_negative_rate{self.metric_suffix}":
                    false_negative_rate(false_negative, true_positive)}
            self.calculated_metrics.update(temp_metrics)
        except Exception:
            self.logger.exception("Unable to calculate confusion matrix metric")
        # calculate metrics that use prediction probabilities as inputs
        if not y_pred_proba:
            return
        try:
            fpr, tpr, _ = sklearn.metrics.roc_curve(y_true, y_pred_proba[:, 1])
            self.calculate_metrics([sklearn.metrics.auc], value_1=fpr,
                                   value_2=tpr)
        except Exception:
            self.logger.exception("Unable to calculate roc_curve")
        # calculate metrics that use y_true and y_pred_proba as inputs
        self.calculate_metrics(
            metrics_list=SKLEARN_CLASSIFIER_PROBABILITY_METRICS,
            value_1=y_true, value_2=y_pred_proba[:, 1])

    def populate_metrics_multiclass(self, y_pred, y_true, y_pred_proba):
        """
        Populate metrics for multiclass classifiers using both sklearn library.
        Returns: metrics values and metrics name
        """
        self.calculate_metrics(SKLEARN_MULTICLASS_CLASSIFIER_METRICS,
                               y_pred, y_true)
        for avg_method in ["micro", "macro", "weighted"]:
            self.calculate_metrics(SKLEARN_MULTICLASS_AVERAGING_METRICS, y_true,
                                   y_pred, avg_method=avg_method)
        if not y_pred_proba:
            return
        for avg_method, config in itertools.product(["macro", "weighted"],
                                                    ["ovr", "ovo"]):
            try:
                self.calculated_metrics[
                    f"{format_metric_key(sklearn.metrics.roc_auc_score)}_" \
                    f"{avg_method}_{config}{self.metric_suffix}"] = round(
                    sklearn.metrics.roc_auc_score(y_true, y_pred_proba,
                                                  average=avg_method,
                                                  multi_class=config), 4)
            except Exception:
                self.logger.exception(f"Unable to calculate roc_aur_score "
                                      f"AvgMethod:{avg_method}, Config:"
                                      f"{config}")

    def populate_metrics_multilabel(self, y_pred, y_true, y_pred_proba,
                                    y_score):
        """
        Populate metrics for multilabel classifiers with sklearn library
        Returns: metrics values and metrics name
        """
        self.calculate_metrics(SKLEARN_MULTILABEL_CLASSIFIER, y_pred, y_true)
        # metrics that needs averaging method
        averaging_metrics = SKLEARN_MULTILABEL_AVERAGING_METRICS
        for avg_method in ["micro", "macro", "weighted", "samples"]:
            self.calculate_metrics([sklearn.metrics.average_precision_score],
                                   y_true, y_pred_proba, avg_method)
            self.calculate_metrics([averaging_metrics], y_true, y_pred,
                                   avg_method)
        self.calculate_metrics(SKLEARN_MULTILABEL_RANKING_METRICS, y_true,
                               y_score)
        # roc_auc_score
        if not y_pred_proba:
            return
        # reshape prediction probability
        y_pred_proba_t = np.transpose(np.array(y_pred_proba)[:, :, 1])
        # roc_auc_score for multilabel
        for avg_method in ["micro", "macro", "weighted", "samples"]:
            try:
                self.calculated_metrics[
                    f"{format_metric_key(sklearn.metrics.roc_auc_score)}_" \
                    f"{avg_method}{self.metric_suffix}"] = round(
                    sklearn.metrics.roc_auc_score(y_true, y_pred_proba_t,
                                                  average=avg_method), 4)
            except Exception:
                self.logger.exception(f"Unable to calculate roc_auc_score "
                                      f"AvgMethod:{avg_method}")

    def plot_precision_recall_curve_binary(self, x_values, y_pred, y_true,
                                           y_score):
        """ Plot precision recall curve for binary classifiers """
        if not y_score:
            return
        try:
            average_precision = sklearn.metrics.average_precision_score(
                y_true, y_score)
            disp_prec = sklearn.metrics.plot_precision_recall_curve(
                self.model, x_values, y_true)
            disp_prec.ax_.set_title(
                f'Precision-Recall curve {self.metric_suffix}' +
                ' : AP={0:0.2f}'.format(average_precision))
            plt.show()
            xpresso_save_plot("Precision_recall_curve",
                              output_path=PLOTS_FOLDER)
        except Exception as exp:
            self.logger.exception(
                f"Unable to plot precision_recall_curve. Reason: {str(exp)}")

    def plot_precision_recall_curve_multi(self, y_pred, y_true,
                                          model_classes, y_pred_proba):
        """
        Plot precision recall curve for multiclass and multilabel classifiers
        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()
            y_true_binarized = label_binarize(y_true, classes=model_classes)

            for i in range(len(model_classes)):
                precision[i], recall[
                    i], _ = sklearn.metrics.precision_recall_curve(
                    y_true_binarized[:, i], y_pred_proba[:, i])
                average_precision[i] = sklearn.metrics.average_precision_score(
                    y_true_binarized[:, i], y_pred_proba[:, i])

            # A "micro-average": quantifying score on all classes jointly
            precision["micro"], recall[
                "micro"], _ = sklearn.metrics.precision_recall_curve(
                y_true_binarized.ravel(), y_pred_proba.ravel())
            average_precision[
                "micro"] = sklearn.metrics.average_precision_score(
                y_true_binarized, y_pred_proba, average="micro")
            # Plot all precision_recall curves
            plt.figure(figsize=(12, 6))
            colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
            lines = []
            labels = []
            l, = plt.plot(recall["micro"], precision["micro"], color='gold',
                          lw=2)
            lines.append(l)
            lines.append(l)
            labels.append('micro-average Precision-recall (area = {0:0.2f})'
                          ''.format(average_precision["micro"]))
            for i, color in zip(range(len(model_classes)), colors):
                l, = plt.plot(recall[i], precision[i], color=color, lw=2)
                lines.append(l)
                labels.append('Precision-recall for class {0} (area = {1:0.2f})'
                              ''.format(i, average_precision[i]))
            fig = plt.gcf()
            fig.subplots_adjust(bottom=0.25)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.title('Precision-Recall curve' + self.metric_suffix +
                      ': micro AP={0:0.2f}'.format(average_precision["micro"]))
            plt.legend(lines, labels, loc="lower left", prop=dict(size=9))
            plt.show()
            xpresso_save_plot("Precision_recall_curve" + self.metric_suffix,
                              output_path=PLOTS_FOLDER)
        except Exception:
            self.logger.exception("Unable to plot precision_recall_curve_multi")

    def plot_roc_curve_binary(self, x_values, y_pred, y_true, y_pred_proba):
        """ Plot roc curve and compute roc area for binary classifiers """
        if not y_pred_proba:
            return

        try:
            average_roc_auc_score = sklearn.metrics.roc_auc_score(y_true,
                                                                  y_pred_proba[
                                                                  :, 1])
            disp_roc = sklearn.metrics.plot_roc_curve(self.model, x_values,
                                                      y_true)
            disp_roc.ax_.set_title('ROC curve' + self.metric_suffix +
                                   ' : AUC ROC={0:0.2f}'.format(
                                       average_roc_auc_score))
            plt.show()
            xpresso_save_plot("ROC_curve" + self.metric_suffix,
                              output_path=PLOTS_FOLDER)
        except Exception:
            self.logger.exception("Unable to plot roc_curve_binary")

    def plot_roc_curve_multi(self, y_pred, y_true, y_score):
        """
        Plot ROC curve and compute ROC area for multiclass and multilabel
        classifiers
        """
        if not y_score:
            return
        try:
            y_true_binarized = label_binarize(y_true,
                                              classes=self.model.classes_)
            fpr = dict()
            tpr = dict()
            roc_auc = dict()
            for i in range(len(self.model.classes_)):
                fpr[i], tpr[i], _ = sklearn.metrics.roc_curve(
                    y_true_binarized[:, i], y_score[:, i])
                roc_auc[i] = sklearn.metrics.auc(fpr[i], tpr[i])
            # Compute micro-average ROC curve and ROC area
            fpr["micro"], tpr["micro"], _ = sklearn.metrics.roc_curve(
                y_true_binarized.ravel(), y_score.ravel())
            roc_auc["micro"] = sklearn.metrics.auc(fpr["micro"],
                                                   tpr["micro"])
            # First aggregate all false positive rates
            all_fpr = np.unique(np.concatenate(
                [fpr[i] for i in range(len(self.model.classes_))]))
            # Then interpolate all ROC curves at this points
            mean_tpr = np.zeros_like(all_fpr)
            for i in range(len(self.model.classes_)):
                mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])
            # Finally average it and compute AUC
            mean_tpr /= len(self.model.classes_)
            fpr["macro"] = all_fpr
            tpr["macro"] = mean_tpr
            roc_auc["macro"] = sklearn.metrics.auc(fpr["macro"], tpr["macro"])
            # Plot all ROC curves
            plt.figure(figsize=(12, 6))
            line_width = 2
            plt.plot(fpr["micro"], tpr["micro"],
                     label='micro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["micro"]),
                     color='deeppink', linestyle=':', linewidth=4)
            plt.plot(fpr["macro"], tpr["macro"],
                     label='macro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["macro"]),
                     color='navy', linestyle=':', linewidth=4)
            colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
            for i, color in zip(range(len(self.model.classes_)), colors):
                plt.plot(fpr[i], tpr[i], color=color, lw=line_width,
                         label='ROC curve of class {0} (area = {1:0.2f})'
                               ''.format(i, roc_auc[i]))
            plt.plot([0, 1], [0, 1], 'k--', lw=line_width)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')
            roc_plot_title = self.model.__class__.__name__ + '_ROC_curve' \
                             + self.metric_suffix
            plt.title(roc_plot_title)
            plt.legend(loc="lower right")
            plt.show()
            xpresso_save_plot(roc_plot_title, output_path=PLOTS_FOLDER)
            self.logger.info("Multiclass/Multilable ROC Curve Saved")
        except Exception:
            self.logger.exception("Unable to plot ROC curve multi")

    def plot_multilabel_confusion_matrix(self, y_pred, y_true):
        """ Plot confusion matrix for multilabel classifiers"""
        try:
            cm_plot_title = ""
            # generate multilabel_confusion_matrix
            cm_arrays = sklearn.metrics.multilabel_confusion_matrix(y_true,
                                                                    y_pred)

            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))
            # plot a confusion matrix for each class
            for i in range(len(cm_arrays)):
                df_cm = pd.DataFrame(cm_arrays[i])
                sn.set(font_scale=1.2)  # for label size
                sn.heatmap(df_cm, annot=True,
                           cmap="Blues", ax=axes[i], fmt='g')
                cm_plot_title = format_metric_key(
                    sklearn.metrics.multilabel_confusion_matrix) + \
                                self.metric_suffix + "_class_" + \
                                str(self.model.classes_[i])
                axes[i].set_xlabel('Predicted label')
                axes[i].set_ylabel('True label')
                axes[i].set_title(cm_plot_title)
            fig.tight_layout(pad=1.0)
            plt.show()
            xpresso_save_plot(cm_plot_title, output_path=PLOTS_FOLDER)
            self.logger.info("Multilable Confusion Matrix Saved")
        except Exception:
            self.logger.exception("Unable to plot multilabel confusion matrix")

    def plot_confusion_matrix(self, x_values, y_pred, y_true):
        """ Plot confusion matrix for binary or multiclass classifiers """
        try:
            cm_disp = sklearn.metrics.plot_confusion_matrix(
                self.model, x_values, y_true,
                display_labels=self.model.classes_, cmap="Blues")
            cm_disp.ax_.set_title("Confusion Matrix" + self.metric_suffix)
            plt.show()
            xpresso_save_plot("Confusion_matrix" + self.metric_suffix,
                              output_path=PLOTS_FOLDER)
            self.logger.info("Confusion Matrix Saved")
        except Exception:
            self.logger.exception("Unable to plot confusion matrix")

    def save_classification_report(self, y_pred, y_true):
        """
        Save classification report(as csv file) for all types of
        classification models
        """
        try:
            cls_report = sklearn.metrics.classification_report(y_true, y_pred,
                                                               output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save model to data folder
            pickle.dump(cls_report_df, open(
                os.path.join(METRICS_FOLDER, f"classification_report"
                                             f"_{self.metric_suffix}.pkl"),
                'wb'))
            self.logger.info("Classification Report Saved")
        except Exception:
            self.logger.exception("Unable to save classification report")

    def populate_metrics_unsupervised_clustering(self, x_values, y_pred,
                                                 y_true):
        """ Populate metrics for unsupervised clustering model"""
        self.calculate_metrics(SKLEARN_UNSUPERVISED_CLUSTERING_METRICS,
                               y_pred, y_true)

        # additional metrics for unsupervised clustering models
        self.calculate_metrics(SKLEARN_CLUSTERING_SPECIFIC_METRICS,
                               x_values, y_pred)

    @ignore_warnings(category=ConvergenceWarning)
    def plot_validation_curves(self, x_values, y_values,
                               param_name="max_iter",
                               param_range=None, scoring_metrics=None,
                               **kwargs):
        """
        Generate validation plots: the test, train scores vs param_name plot
        While default parameters (param_name) for the plot is max_iter,
        users have the options to plot validation curve for any parameters of
        the estimator provided.
        Args:
            x_values : array-like, shape (n_samples, n_features) feature matrix
            y_values : array-like, shape (n_samples) or (n_samples, n_classes)
                target variable
            param_name : str, any parameter of the estimator the user wants
                to evaluate default value is max_iter
            param_range : list, range of parameter values default value is
                range[1,20]
            scoring_metrics: str or list of str, scoring parameters
                pre-defined in sklearn, e.g.,"accuracy" see section "The
                scoring parameter: defining model evaluation rules" within
                sklearn documentation default for regressor is
                "neg_mean_squared_error", "neg_root_mean_squared_error"
                default for classifier is "accuracy", "neg_log_loss"
            **kwargs includes: n_jobs , cv , pre_dispatch , verbose, error_score
                n_jobs-default None, optional
                cv-default 5,  optional
                pre_dispatch-default None, optional
                verbose-int, default=0
                error_score-‘raise’ or numeric, default=np.nan
        """

        # use default metrics if no values provided to scoring_metrics by
        # end-users
        if not scoring_metrics:
            if sklearn.base.is_regressor(self.model):
                scoring_metrics = ["neg_mean_absolute_error",
                                   "neg_root_mean_squared_error"]
            elif sklearn.base.is_classifier(self.model):
                scoring_metrics = ["accuracy", "neg_log_loss"]

        # if end-users provide only one metrics
        elif isinstance(scoring_metrics, str):
            scoring_metrics = [scoring_metrics]

        # if no param_range is provided, param_range is list of integers
        # range from 1 to 20 for max_iter
        if param_range is None:
            param_range = list(range(1, 21))

        fig, axes = plt.subplots(len(scoring_metrics), 1, figsize=(6, 8))

        # plot learning curve for each scoring metrics
        for i, sc_metric in enumerate(scoring_metrics):
            try:

                train_scores, test_scores = validation_curve(
                    estimator=self.model, X=x_values, y=y_values,
                    param_name=param_name, param_range=param_range,
                    scoring=sc_metric, **kwargs)

                train_scores_mean = np.mean(train_scores, axis=1)
                train_scores_std = np.std(train_scores, axis=1)
                val_scores_mean = np.mean(test_scores, axis=1)
                val_scores_std = np.std(test_scores, axis=1)

                cv_results_df = pd.DataFrame(
                    {param_name: param_range,
                     "val_scores_mean": val_scores_mean,
                     "train_scores_mean": train_scores_mean})
                val_score_best = val_scores_mean.max()
                max_iters_best = cv_results_df.loc[
                    cv_results_df.val_scores_mean == val_score_best,
                    param_name].values[0]

                # Plot score vs max_iters
                axes[i].grid()
                axes[i].fill_between(param_range,
                                     train_scores_mean - train_scores_std,
                                     train_scores_mean + train_scores_std,
                                     alpha=0.1, color="y")
                axes[i].fill_between(param_range,
                                     val_scores_mean - val_scores_std,
                                     val_scores_mean + val_scores_std,
                                     alpha=0.1, color="g")
                axes[i].plot(param_range, train_scores_mean, 'o-', color="y",
                             label="Training score")
                axes[i].plot(param_range, val_scores_mean, 'o-', color="g",
                             label="Cross-validation score")
                axes[i].plot(max_iters_best, val_score_best, 'r*',
                             label='Best Param : ' + str(max_iters_best))
                axes[i].set_xlabel(param_name)
                axes[i].set_ylabel(scoring_metrics[i])
                axes[i].set_title(f"Validation curve : Train/Val Scores vs."
                                  f" {param_name}")
                axes[i].legend(loc="best")
            except Exception:
                self.logger.exception("Unable to plot validation curves")

        fig.tight_layout(pad=3.0)
        plt.show()
        xpresso_save_plot(f"validation_curve_{self.model.__class__.__name__}",
                          output_path=PLOTS_FOLDER)
        self.logger.info("Learning curve plot saved")

    def explain_shap(self, x_values, shap_explainer=None,
                     shap_explaining_set_size=None,
                     shap_n_individual_instance=1, shap_n_top_features=5,
                     shap_n_top_interaction_features=3,
                     shap_link="identity",
                     shap_feature_names=None):
        """
        Create Shapley plots for the trained model.
        Allows an approximation of shapley values for a given model based on
        a number of samples from data source X
        Args:
            x_values (dataframe, numpy array, or pixel_values) : dataset to
                be explained
            shap_explainer (string, optional): choose one of explainer in
             the following list or if leave as None, the method will try each
             explainer in the list in order until it reaches one that that is
             compatible with the given model type {"TreeExplainer",
             "LinearExplainer", "DeepExplainer","GradientExplainer",
             "KernelExplainer", "SamplingExplainer"}
            shap_feature_names (list of strings, optional) : a list of column
                names
            shap_explaining_set_size(int, optional): Number of samples in X
                to be explained by SHAP.
            shap_n_individual_instance (int, optional): Number of individual
                instance(s)/sample(s)/observation(s) in the explaining set to
                be randomly chosen to create SHAP Force Plots and SHAP
                Decision Plots.  Default is 1.
            shap_n_top_features(int, optional): Number of top features (by
                SHAP Values) to create SHAP Dependence Plots. If values is
                set to -1, all independent features will be used (one SHAP
                Dependence Plot for each feature). Default is 5.
            shap_n_top_interaction_features(int, optional): Number of top
                features (by SHAP Interaction Values) to create SHAP
                Interaction Values Dependence Plot (only supporting
                TreeExpaliner for now ). If values is set to -1,
                all independent features will be used (one SHAP Interaction
                Values Dependence Plot for each pair of features). Default is 3.
            shap_link (string, optional) : default is 'identity' or use
                'logit' to transform log odds to probabilities
            Returns:
                Returns: SHAP plots
        """
        if not isinstance(self.model, sklearn.base.BaseEstimator):
            self.logger.error("Model not built or invalid type")
            return
        print("Generating SHAP explainability plots")
        # generate explainer based on model type
        self.explainer = self.generate_explainer(x_values, shap_explainer)

        # end the program if the explainer is None
        if not self.explainer:
            return

        # print the type of SHAP explainer used if explainer generated
        # successfully
        print("SHAP Explainer used : ", return_name(self.explainer))

        # assign variables
        # define number of top features to plot
        if shap_n_top_features == -1:
            shap_n_top_features = x_values.shape[1]

        # define number of top features to plot interaction values plots
        if shap_n_top_interaction_features == -1:
            shap_n_top_interaction_features = x_values.shape[1]

        # generate explaining set
        x_explain = self.generate_explaining_set(x_values,
                                                 shap_explaining_set_size)

        # compute SHAP value, expected value, interaction values(if support)
        shap_values = self.explainer.shap_values(x_explain)
        expected_value = self.explainer.expected_value
        try:
            shap_interaction_values = self.explainer.shap_interaction_values(
                x_explain)
        except Exception:
            shap_interaction_values = None

        # obtain feature names
        feature_names = shap_generate_feature_names(x_values,
                                                    shap_feature_names)
        # check if shap_values have multiple elements, as a result of a
        # multioutput model
        if not isinstance(shap_values, list):
            multioutput_n = EMPTY_STRING
            shap_plots_single_output(
                shap_values=shap_values, expected_value=expected_value,
                shap_interaction_values=shap_interaction_values,
                x_explain=x_explain, feature_names=feature_names,
                multioutput_n=multioutput_n, link=shap_link,
                shap_n_top_features=shap_n_top_features,
                shap_n_individual_instance=shap_n_individual_instance,
                shap_n_top_interaction_features=shap_n_top_interaction_features,
                output_path=SHAP_FOLDER)
            return
        n_outputs = len(shap_values)
        for i in range(n_outputs):
            # provide a label for the output
            multioutput_n = f"multioutput_{i}"
            shap_interaction_values_i = None
            if shap_interaction_values:
                shap_interaction_values_i = shap_interaction_values[i]
            shap_plots_single_output(
                shap_values=shap_values[i], expected_value=expected_value[i],
                shap_interaction_values=shap_interaction_values_i,
                x_explain=x_explain, feature_names=feature_names,
                multioutput_n=multioutput_n, link=shap_link,
                shap_n_top_features=shap_n_top_features,
                shap_n_individual_instance=shap_n_individual_instance,
                shap_n_top_interaction_features=shap_n_top_interaction_features,
                output_path=SHAP_FOLDER)

    def generate_explaining_set(self, x_values, shap_explaining_set_size):
        """Generate SHAP explaining set"""
        # Define SHAP explaining set size
        if shap_explaining_set_size:
            n_explain = shap_explaining_set_size
        # if user doesn't provide number of data to explain
        else:
            # For Kernel explainer, use up to 1000 samples to speed up the
            # process as
            if isinstance(self.explainer, shap.KernelExplainer):
                n_explain = min(1000, x_values.shape[0])
            else:
                n_explain = x_values.shape[0]

        # prepare data
        # if X is a dataframe, convert X to numpy array
        if isinstance(x_values, pd.DataFrame):
            x_values = x_values.to_numpy()
        # obtain explaining set (x matrix to be explained by SHAP)
        x_explain_ind = np.random.choice(x_values.shape[0],
                                         min(n_explain, x_values.shape[0]),
                                         replace=False)
        return x_values[x_explain_ind]

    def generate_explainer(self, x_values, shap_explainer):
        """Generate SHAP explainer"""
        # convert background data as numpy array,
        # Based on SHAP, anywhere from 100 to 1000 random background samples
        # are good sizes to use
        if isinstance(x_values, pd.DataFrame):
            x_values = x_values.to_numpy()
        bdata = x_values[
            np.random.choice(x_values.shape[0], min(1000, x_values.shape[0]),
                             replace=False)]
        # Listing of all SHAP Explainers
        shap_explainer_dict = {
            'TreeExplainer': shap.TreeExplainer,
            'LinearExplainer': shap.LinearExplainer,
            'DeepExplainer': shap.DeepExplainer,
            'GradientExplainer': shap.GradientExplainer,
            'KernelExplainer': shap.KernelExplainer,
            'SamplingExplainer': shap.SamplingExplainer}
        if shap_explainer:
            if shap_explainer in ["SamplingExplainer", "KernelExplainer"]:
                # if using KernelExplainer, the size of background data is
                # limited to 100 otherwise shap gives a warning
                if shap_explainer == "KernelExplainer":
                    bdata = bdata[:100]
                try:
                    explainer = shap_explainer_dict[shap_explainer](
                        self.model.predict, bdata)
                    return explainer
                except Exception as exp:
                    self.logger.exception(f"Reason: {str(exp)}")
            else:
                try:
                    explainer = shap_explainer_dict[shap_explainer](self.model,
                                                                    bdata)
                    return explainer
                except Exception as exp:
                    self.logger.exception(f"Reason: {str(exp)}")
        # if user doesn't provide a SHAPExplainer of choice or the
        # SHAPExplainer provided by user is not compatible with
        # the model, try other SHAPexplainers in the order of the list
        for i in shap_explainer_dict:
            # for any other type explainer other than Sampling and Kernel
            if i not in ["SamplingExplainer", "KernelExplainer"]:
                try:
                    explainer = shap_explainer_dict[i](self.model, bdata)
                    return explainer
                except Exception as exp:
                    self.logger.exception(f"Reason: {str(exp)}")
                continue
            # if using KernelExplainer, the size of background data is
            # limited to 100 otherwise shap gives a warning
            if shap_explainer == "KernelExplainer":
                bdata = bdata[:100]
            try:
                explainer = shap_explainer_dict[i](self.model.predict, bdata)
                return explainer
            except Exception as exp:
                self.logger.exception(f"Reason: {str(exp)}")
        return None

